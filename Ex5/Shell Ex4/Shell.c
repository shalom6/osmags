#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "LineParser.h"

#define SUCCESS 0
/* Boolean */
#define FALSE 0
#define TRUE (!FALSE)

/* Size */
#define PATH_MAX 1024
#define COMMAND_LENGTH 2048

/* Values */
#define STR_CD "cd"
#define STR_MYECHO "myecho"

void execute(cmdLine *pCmdLine);
void pipeHandle(cmdLine* pCmdLine);
void runCommand(cmdLine *pCmdLine);
void changeCurrentDirectory(char* dirPath);
void myEcho(char* const* arguments, int argCount);

int main()
{
    char dirName[PATH_MAX];
    char cmdLineStringToParse[COMMAND_LENGTH];

    cmdLine* parsedCmdLine = NULL;

    int isFirstLoop = TRUE;

    while(isFirstLoop || (parsedCmdLine && strcmp(parsedCmdLine->arguments[0], "quit")))
    {
        getcwd(dirName, PATH_MAX);
        strcat(dirName, "$ ");
        printf("%s", dirName);
        fgets(cmdLineStringToParse, COMMAND_LENGTH, stdin);
        parsedCmdLine = parseCmdLines(cmdLineStringToParse);

        if(parsedCmdLine != NULL)
        {
            if(strcmp(parsedCmdLine->arguments[0], "quit"))
            {
                execute(parsedCmdLine);
                freeCmdLines(parsedCmdLine);
            }

            isFirstLoop = FALSE;
        }
        else
           isFirstLoop =  TRUE;
    }

    return (SUCCESS);
}

void execute(cmdLine *pCmdLine)
{

    if (strcmp(pCmdLine->arguments[0], STR_CD) == 0)
    {
        if(pCmdLine->argCount != 2)
        {
            printf("Wrong Usage Of CD\n");
        }
        else
        {
            changeCurrentDirectory(pCmdLine->arguments[1]);
        }
    }
    else if(strcmp(pCmdLine->arguments[0], STR_MYECHO) == 0)
    {
        myEcho(pCmdLine->arguments, pCmdLine->argCount);
    }
    else
    {
        if(pCmdLine->next)
        {
            pipeHandle(pCmdLine);
        }
        else
        {
            runCommand(pCmdLine);
        }
    }
}

void pipeHandle(cmdLine* pCmdLine)
{
    int pipefd[2];
    int pipeStatus;

    pipeStatus = pipe(pipefd);

    if(pipeStatus == (-1)) /* Error */
    {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    pid_t cpid1 = fork();

    if(cpid1 == 0) /* This code is running on the child1 side*/
    {
        close(1); /* Close normal stdout */

        dup(pipefd[1]); /* make stdout same as pipefd[1] */
        close(pipefd[0]); /* close the pipe input that we don't need */
        runCommand(pCmdLine);

        exit(EXIT_SUCCESS);
    }
    else
    {
        close(pipefd[1]);

        pid_t cpid2 = fork();

        if(cpid2 == 0) /* This code is running on the child2 side*/
        {
            close(0); /* Close normal stdin */
            dup(pipefd[0]); /* make stdin same as pipefd[0] */
            close(pipefd[1]); /* close the pipe output that we don't need */

            runCommand(pCmdLine->next);
            exit(EXIT_SUCCESS);
        }
        else
        {
            close(pipefd[0]);

            waitpid(-1, &cpid1, 0);
            waitpid(-1, &cpid2, 0);
        }
    }
}

void runCommand(cmdLine *pCmdLine)
{
    pid_t child_pid = fork();

    int fdIn = -1;
    int fdOut = -1;

    if(0 == child_pid) /* In the child process*/
    {
        if(pCmdLine->inputRedirect)
        {
            close(0); // close the standart input
            fdIn = open(pCmdLine->inputRedirect, O_RDONLY);
        }

        if(pCmdLine->outputRedirect)
        {
            close(1); // close the standart output
            fdOut = open(pCmdLine->outputRedirect, O_WRONLY);
        }

        int execRetStatus = execvp(pCmdLine->arguments[0], (pCmdLine->arguments));

        if(execRetStatus == (-1))
        {
            perror("An Error has occured");
        }

        close(fdIn);
        close(fdOut);

        exit(0);
    }
    else
    {
        int status;
        int retWait = (int) waitpid(-1, &status, 0);
    }

}

void changeCurrentDirectory(char* dirPath)
{
    int stat = chdir(dirPath);

    if(stat != 0)
    {
        printf("There was a problem changing directory\n");
    }
}

void myEcho(char* const* arguments, int argCount)
{
    int i;

	for(i = 1; i < argCount; ++i)
    {
         printf("%s ", arguments[i]);
    }

    printf("\n");
}



