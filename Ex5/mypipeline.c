#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define SUCCESS 0
#define BUFF_LEN 11

int main()
{
    int pipefd[2];
    int pipeStatus;

    pipeStatus = pipe(pipefd);

    if(pipeStatus == (-1)) /* Error */
    {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    pid_t cpid1 = fork();

    if(cpid1 == 0) /* This code is running on the child1 side*/
    {
        char* args1[] = { "ls", "-l", NULL };

        close(1); /* Close normal stdout */

        dup(pipefd[1]); /* make stdout same as pipefd[1] */
        close(pipefd[0]); /* close the pipe input that we don't need */
        execvp(args1[0], args1);

        exit(EXIT_SUCCESS);
    }
    else
    {
        close(pipefd[1]);

        pid_t cpid2 = fork();

        if(cpid2 == 0) /* This code is running on the child2 side*/
        {
            char* args2[] = { "tail",  "-n", "2", NULL};

            close(0); /* Close normal stdin */
            dup(pipefd[0]); /* make stdin same as pipefd[0] */
            close(pipefd[1]); /* close the pipe output that we don't need */

            execvp(args2[0], args2);
            exit(EXIT_SUCCESS);
        }
        else
        {
            close(pipefd[0]);

            waitpid(-1, &cpid1, 0);
            waitpid(-1, &cpid2, 0);
        }
    }

    printf("\n Back in the parent proccess\n");

    return(SUCCESS);
}
