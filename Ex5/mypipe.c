#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define SUCCESS 0
#define BUFF_LEN 11

int main()
{
    int pipefd[2];
    int pipeStatus;
    pid_t cpid;
    char* buf = " magshimim ";

    pipeStatus = pipe(pipefd);

    if(pipeStatus == (-1)) /* Error */
    {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    cpid = fork();

    if(cpid == 0) /* This code is running on the child side*/
    {
        close(pipefd[0]); /* close the child ability to read (not needed) */

        write(pipefd[1], buf, BUFF_LEN);
        exit(EXIT_SUCCESS);
    }
    else
    {
        /* close the pipe (read and write) */

        char parBuf[BUFF_LEN];

        read(pipefd[0], parBuf, BUFF_LEN);
        printf("%s\n", parBuf);

        close(pipefd[0]);
        close(pipefd[1]);



        int status;
        waitpid(-1, &status, 0);
    }

    printf("\n Back in the parent proccess\n");

    return(SUCCESS);
}
