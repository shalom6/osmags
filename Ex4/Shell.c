#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "LineParser.h"

#define SUCCESS 0
/* Boolean */
#define FALSE 0
#define TRUE (!FALSE)

/* Size */
#define PATH_MAX 1024
#define COMMAND_LENGTH 2048

/* Values */
#define STR_CD "cd"
#define STR_MYECHO "myecho"

void execute(cmdLine *pCmdLine);
void runCommand(cmdLine *pCmdLine);
void changeCurrentDirectory(char* dirPath);
void myEcho(char* const* arguments, int argCount);

int main()
{
    char dirName[PATH_MAX];
    char cmdLineStringToParse[COMMAND_LENGTH];

    cmdLine* parsedCmdLine = NULL;

    int isFirstLoop = TRUE;

    while(isFirstLoop || strcmp(parsedCmdLine->arguments[0], "quit"))
    {
        getcwd(dirName, PATH_MAX);
        strcat(dirName, "$ ");
        printf("%s", dirName);

        fgets(cmdLineStringToParse, COMMAND_LENGTH, stdin);
        parsedCmdLine = parseCmdLines(cmdLineStringToParse);

	if(strcmp(parsedCmdLine->arguments[0], "quit"))
	{
		execute(parsedCmdLine);
		freeCmdLines(parsedCmdLine);
	}

        isFirstLoop = FALSE;
    }
    return (SUCCESS);
}

void execute(cmdLine *pCmdLine)
{
    if (strcmp(pCmdLine->arguments[0], STR_CD) == 0)
    {
        if(pCmdLine->argCount != 2)
        {
            printf("Wrong Usage Of CD\n");
        }
        else
        {
            changeCurrentDirectory(pCmdLine->arguments[1]);
        }
    }
    else if(strcmp(pCmdLine->arguments[0], STR_MYECHO) == 0)
    {
        myEcho(pCmdLine->arguments, pCmdLine->argCount);
    }
    else
    {
        runCommand(pCmdLine);
    }

}

void runCommand(cmdLine *pCmdLine)
{
    pid_t child_pid = fork();

    if(0 == child_pid)
    {
        int execRetStatus = execvp(pCmdLine->arguments[0], (pCmdLine->arguments));

        if(execRetStatus == (-1))
    	{
        	perror("An Error has occured");
    	}

        exit(0);
    }
    else
    {
        int status;
        int retWait = (int)waitpid(-1, &status, 0);
    }
}

void changeCurrentDirectory(char* dirPath)
{
    int stat = chdir(dirPath);

    if(stat != 0)
    {
        printf("There was a problem changing directory\n");
    }
}

void myEcho(char* const* arguments, int argCount)
{
    int i;

	for(i = 1; i < argCount; ++i)
    {
         printf("%s ", arguments[i]);
    }

    printf("\n");
}








