section .rodata
MSG:	DB	"welcome to sortMe, please sort me",10,0
S1:	DB	"%d",10,0 ; 10 = '\n' , 0 = '\0'

section .data

array	DB 5,1,7,3,4,9,12,8,10,2,6,11	; unsorted array
len	DB 12	
  
section .text
	align 16
	global main
	extern printf

main:
	push MSG	; print welcome message
	call printf
	add esp,4	; clean the stack 
	
	call printArray ;print the unsorted array

;	write your code here
;	you can add functions at the end of this file,
;	and call them from here
	
	mov ebx, array
	inc ebx
	
	mov ecx, len ;counter of the for loop cx
	dec ecx

for_loop:
	mov al, byte [ebx]
	mov esi, ebx

	while_loop:
		dec esi
		cmp [esi], al
		jg swap
		jmp stop_while ; if(a[i-1] > a[i])

		swap:
			mov dl, al
			mov al, [esi]
			mov [esi], dl
			inc esi
			mov [esi], al
			dec esi
		cmp esi, 0
		jg while_loop			
	stop_while:
		
	inc ebx
	dec ecx
	jnz for_loop

	
call printArray


	mov eax, 1 	;exit system call
	int 0x80

printArray:
	push ebp	;save old frame pointer
	mov ebp,esp	;create new frame on stack
	pusha		;save registers

	mov eax,0
	mov ebx,0
	mov edi,0

	mov esi,0	;array index
	mov bl,byte[len]
	add edi,ebx	; edi = array size

print_loop:
	cmp esi,edi
	je print_end
	mov al ,byte[array+esi]	;set num to print in eax
	push eax
	push S1
	call printf
	add esp,8	;clean the stack
	inc esi
	jmp  print_loop
print_end:
	popa		;restore registers
	mov esp,ebp	;clean the stack frame
	pop ebp		;return to old stack frame
	ret
