#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>

#define SUCCESS 0
#define ONE_SEC 1

#define INPUT 0
#define OUTPUT 1

void child_exit_handle_stop();

int main()
{
    int pipefd[2];
    int pipeStatus;

    pipeStatus = pipe(pipefd);

    if(pipeStatus == (-1)) /* Error */
    {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    close(OUTPUT);
    open("output.txt", O_WRONLY | O_APPEND);

    pid_t cpid = fork();

    if(cpid == 0) /* This code is running on the child side*/
    {
        signal(SIGSTOP, child_exit_handle_stop);

        while(1)
            printf("I'm still alive\n");

    }
    else
    {
        sleep(ONE_SEC);
        printf("\nDispatching\n");
        kill(cpid, SIGSTOP);
        printf("Dispatched\n");
        exit(EXIT_SUCCESS);

    }

    //close(0);

    return(SUCCESS);
}

void child_exit_handle_stop()
{
    exit(EXIT_SUCCESS);
}
