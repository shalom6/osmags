#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>

#define SUCCESS 0
#define IN_LENGTH 1024

#define INPUT 0
#define OUTPUT 1

void quit(int signum);

/***
Quit/Stop signals values:
* SIGINT - 2
* SIGQUIT - 3
* SIGTERM - 15
*/

int main()
{
    int x;

    signal(SIGINT, quit);
    signal(SIGQUIT, quit);
    signal(SIGTERM, quit);

    for(x = 0; x < 2000000000; ++x)
    {
        printf("I'm stiil alive i = %d\n", x);
    }

    exit(EXIT_SUCCESS);

    return(SUCCESS);
}

void quit(int signum)
{
    const char QUIT_CHAR_1 = 'y';
    const char QUIT_CHAR_2 = 'Y';

    char ans[IN_LENGTH];
    int outDes = (-1);


    close(OUTPUT);
    outDes = open("output2.txt", O_WRONLY | O_APPEND);

    printf("Do you want to quit? (press y to quit or any other key to continue)\n ");
    scanf("%s", ans);

    if(ans[0] == QUIT_CHAR_1 || QUIT_CHAR_2 == ans[0])
    {
        exit(EXIT_SUCCESS);
    }

    close(OUTPUT);
}









